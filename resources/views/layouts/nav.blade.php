@include('layouts.header')
<nav class="navbar navbar-expand-lg navbar-light">
    <div class="container">
        <ul class="navbar-nav" style="">
            <li class="nav-item active">
                <a class="nav-link text-danger hot-sale-div" href="{{url('/')}}">Hot sale offers</a>
            </li>

            <li class="nav-item dropdown lang-div">

                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Language
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown3">
                    <a class="dropdown-item" href="#">English</a>
                    <a class="dropdown-item" href="#">Arabic</a>
                </div>
            </li>
            <li class="nav-item dropdown currency-div">

                <a class="nav-link dropdown-toggle " href="#" id="navbarDropdown3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Currency
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown3">
                    <a class="dropdown-item" href="#">Dollar</a>
                    <a class="dropdown-item" href="#">Pound</a>
                </div>
            </li>
        </ul>
    </div>
</nav>


<div class="row">
    <div class="col logo-div" style="margin-left:15%">
        <h2><strong><a class="nav-link brand" href="{{url('/')}}"><span class="yellow">CACTUS</span> SHOP</a></strong></h2>
        <h6 class="brand-comment">The best online shopping platform</h6>
    </div>

    {{--<div class="col search-col" style="">--}}
        {{--<form class="form-group">--}}
            {{--<ul style="list-style-type: none;margin: 0;padding: 0;overflow: hidden">--}}
                {{--<li style="float: left">--}}
                    {{--<input class="form-control" type="search" placeholder="Search ..." aria-label="Search">--}}
                {{--</li>--}}
                {{--<li style="float: left">--}}
                    {{--<button class="btn btn-outline-success float-right" type="submit">Search</button>--}}
                {{--</li>--}}
            {{--</ul>--}}
        {{--</form>--}}
    {{--</div>--}}

    <div class="input-group w-25 searchdiv ">
        <input type="text" class="form-control" placeholder="Search .." aria-label="Username" aria-describedby="basic-addon1">
        <div class="input-group-append">
            <span class="input-group-text" id="basic-addon1"><i class="fa fa-search"></i></span>
        </div>
    </div>

</div>


<nav class="navbar navbar-expand-lg navbar-light blue">
    <div class="container">

        <div class="nav-item dropdown" id="">
            <a class="nav-link dropdown-toggle text-white bg-light-green categ-nav" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Cactus Categories
            </a>
            <div class="dropdown-menu dropright " aria-labelledby="navbarDropdown">
                <a class="dropdown-item text-primary" href="#">Clothes</a>
                <a class="dropdown-item text-primary" href="#">Electronics</a>
                <a class="dropdown-item text-primary" href="#">Furniture</a>
                <a class="dropdown-item text-primary" href="#">Food</a>

                <a class="nav-link dropdown-toggle text-primary" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Kids</a>
                <div class="dropdown-menu headMenu " aria-labelledby="navbarDropdown2">
                    <a class="dropdown-item text-primary" href="#">Toys</a>
                    <a class="dropdown-item text-primary" href="#">Food</a>
                    <a class="dropdown-item text-primary" href="#">tools</a>
                </div>
            </div>
        </div>

        <div class="search-dest">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Large modal</button>

            <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        ...
                    </div>
                </div>
            </div>
        </div>

        {{--<div class="account-dest"></div>--}}

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse blue" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link text-white" href="Route('home')">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white" href="#">New Products</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled text-white" href="#">About Us</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled text-white" href="#">Blog</a>
                </li>
            </ul>


            <div class=" col account-div" style="margin-right:15%">
                <a class="nav-link dropdown-toggle pull-right float-right" href="#" id="Dropdown3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="fa fa-user"></span> Account
                </a>
            {{--<div class="dropdown-menu" aria-labelledby="Dropdown3">--}}
            {{--@if (Route::has('login'))--}}
            {{--<div class="top-right links">--}}
            {{--@auth--}}
            {{--<a href="{{ url('/home') }}">Home</a>--}}
            {{--@else--}}
            {{--<a href="{{ route('login') }}">Login</a>--}}

            {{--@if (Route::has('register'))--}}
            {{--<a href="{{ route('register') }}">Register</a>--}}
            {{--@endif--}}
            {{--@endauth--}}
            {{--</div>--}}
            {{--@endif--}}
            {{--</div>--}}
            <!-- Authentication Links -->
                <div class="dropdown-menu" aria-labelledby="Dropdown3">
                    @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        @endguest
                </div>
            </div>

        <div class="nav-item float-right">
            <a class=" disabled text-white cart " href="#"><i class="fa fa-shopping-cart"></i></a>
            <span class="cart-products-count" href="#">0</span>
        </div>

    </div>
    </div>
</nav>