<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layouts.header')
    @include('layouts.nav')

</head>
<body>

        <main class="py-4">
            @yield('content')
        </main>
    </div>

    @include('layouts.footer')
        @include('layouts.scripts')

</body>
</html>
