<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->string('color');
            $table->string('serial_number');
            $table->integer('price');
            $table->string('country')->nullable();
            $table->string('brand')->nullable();
            $table->string('model')->nullable();
            $table->integer('posted_by');
            $table->integer('stock')->default(1);
            $table->integer('views')->default(1);
            $table->integer('sold_items')->default(0);
            $table->integer('discount')->default(0);
            $table->integer('likes')->default(0);
//            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
