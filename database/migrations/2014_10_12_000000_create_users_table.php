<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('address')->nullable();
            $table->string('country')->nullable();
            $table->integer('phone_no')->nullable();
            $table->integer('card_no')->nullable();
            $table->integer('security_no')->nullable();
            $table->tinyInteger('is_admin')->default(0);
            $table->string('avatar_url')->nullable();
            $table->string('bio')->nullable();
            $table->string('birth_date');
            $table->tinyInteger('disabled')->default(0);
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
