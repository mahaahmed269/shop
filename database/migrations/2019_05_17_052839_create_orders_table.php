<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->boolean('is_paid')->default(false);
            $table->integer('payment_method');
            $table->string('shipping_address')->nullable();
            $table->dateTime('shipping_date')->nullable();
            $table->decimal('shipping_fees')->nullable();
            $table->string('receiver_name')->nullable();
            $table->integer('receiver_phone')->nullable();
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
